<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 05. 22.
 * Time: 9:29
 */
$secret_key = '_!S3cr3t-k3y!';
//admin menu
$adminmenu = [
    1 => [
        'title' => 'Dashboard',
        'fa-icon' => 'fa fa-dashboard',
        'modulname' => 'main',
        'priv' => 1,
    ],
    9 => [
        'title' => 'Adminisztrátorok',
        'fa-icon' => 'fa fa-user',
        'modulname' => 'admins',
        'priv' => 10,
    ],
];