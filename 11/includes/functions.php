<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 05. 22.
 * Time: 11:10
 */

/**
 * Beléptetés eljárás
 */
function login($email,$pass){
    global $table_prefix,$link,$secret_key;
    $qry = "SELECT  id,email,username FROM {$table_prefix}admins WHERE email = '$email' AND pass ='$pass' AND status=1 LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    if (!$row) {//nem volt ilyen sor, visszatérünk false al
        return false;
    }
        //belépési adatok eltárolása
        $_SESSION['userdata'] = [
            'id' => $row['id'],
            'email' => $row['email'],
            'username' => $row['username'],
        ];
        $_SESSION['sid'] = session_id();
        //user id,email
        //munkafolyamat generált jelszó
        $spass = md5($row['id'] . $_SESSION['sid'] . $secret_key);
        $now = time();//belépés ideje
        $qry = "INSERT INTO sessions(sid,spass,stime) 
                VALUES('{$_SESSION['sid']}','$spass',$now)";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        return true;

}
/**
 * beléptelés ellenőrzése
 * @return bool
 */
function auth(){
    //secret key globálissá tétele hogy lássuk
    global $secret_key,$link;
    //sid,userid,secret_key
    $now = time();//'oldalbetöltés ideje'
    $sid = session_id();
    $expiration_time=15;//in minutes
    $expired = $now - $expiration_time * 60; //most - 15 * 60mp
    if(array_key_exists('userdata',$_SESSION)) {
        $spass = md5($_SESSION['userdata']['id'] . $sid . $secret_key);
    }else{
        $spass = 'failed';
    }
    //takarítás :), lejárt munkamenetek törlése a db ből
    mysqli_query($link,"DELETE FROM sessions WHERE stime < $expired");
    //belépés ellenőrzése a meglévő adatokból
    $qry = "SELECT sid FROM sessions WHERE spass = '$spass' AND stime > $expired ";
    $result = mysqli_query($link,$qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    //var_dump($sid,$row[0]);
    if($sid !== $row[0]){
        //ha nem egyeznek gáz van, ezért 'takarítunk'
        unset($_SESSION);
        session_destroy();
        mysqli_query($link,"DELETE FROM sessions WHERE sid = '$sid' LIMIT 1");//ha 'beragadt' belépés lenne
        return false;
    }
    //sikeres autentikáció, updateljük a stimeot
    mysqli_query($link,"UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1");
    return true;
}

/**
 * Kilépés
 */
function logout(){
    global $link;
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' LIMIT 1");
    //roncsoljuk a sessiont
    $_SESSION=[];
    session_destroy();
    return;
}