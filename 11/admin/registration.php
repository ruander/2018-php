<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 05. 15.
 * Time: 11:43
 */
include '../includes/connect.php';//db csatlakozás, de most a file, amit bekötünk egy mappával feljebb található egy másik mappában
include '../includes/settings.php';


//url paraméterek kinyerése
$action = filter_input(INPUT_GET, 'action');
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

$hiba = [];//üres hiba tömb
if (filter_input(INPUT_POST, 'submit')) {
    echo '<pre>' . var_export($_POST, true) . '</pre>';//ez van a post tömbben

    //név űrlap elem hibakezelése
    $username = filter_input(INPUT_POST, 'username');
    if ($username == "") {
        $hiba['username'] = '<span class="alert alert-danger"> Kötelező kitölteni!</span>';
    }

    //email ürlap elem hibakerelése (formátum és üresség)
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="alert alert-danger"> Hibás adatformátum!</span>';
    } else {

        //nézzük meg van e ilyen már eltárolva
        $qry = "SELECT id FROM admins WHERE email ='$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if ($row[0]) {
            $hiba['email'] = '<span class="alert alert-danger"> Ez az email már szerepel az adatbázisunkban!</span>';
        }
    }

    //jelszó mező hibakezelése (min 6 karakter)
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if (mb_strlen($pass, 'utf8') < 6) {
        $hiba['pass'] = '<span class="alert alert-danger"> Túl kevés karakter!</span>';
    } elseif ($pass != $repass) {
        $hiba['repass'] = '<span class="alert alert-danger"> A jelszavak nem egyeztek!</span>';
    } else {
        $pass = md5($pass . $secret_key);
    }

    //ha üres a hibatömb, akkor nem volt hiba
    if (empty($hiba)) {
        //minden oké
        $now = date('Y-m-d H:i:s');
        $qry = "INSERT INTO 
    `admins` ( 
      `username`, 
      `email`, 
      `pass`, 
      `status`, 
      `time_created`, 
      `time_updated`) 
      VALUES ( 
      '$username', 
      '$email', 
      '$pass', 
      '1', 
      '$now', 
      '$now')";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        $last_insert_id = mysqli_insert_id($link);
        die('sikeres admin felvitel, az azonosító:' . $last_insert_id);
    }
}
?><!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Adminisztrátor regisztráció</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <?php

            //űrlap összeállítása
            $form = '<form method="post">
    <fieldset>
        <legend>Személyes adatok:</legend>
        <div class="form-group">';
            //username
            $form .= '<label for="username">Név*</label>
        <input class="form-control" type="text" name="username" id="username" value="' . (isset($username) ? $username : '') . '" placeholder="John Doe">';
            $form .= isset($hiba['username']) ? $hiba['username'] : "";
            //email
            $form .= '<label for="email">Email*</label>
        <input class="form-control" type="text" name="email" id="email" value="' . filter_input(INPUT_POST, 'email') . '">';
            $form .= isset($hiba['email']) ? $hiba['email'] : "";
            //pass
            $form .= '<label for="pass">Jelszó*</label>
        <input class="form-control" type="password" name="pass" id="pass" value=""
               required>';
            $form .= array_key_exists('pass', $hiba) ? $hiba['pass'] : "";
            //pass retype
            $form .= '<label for="repass">Jelszó újra*</label>
        <input class="form-control" type="password" name="repass" id="repass" value=""
               required>';
            $form .= array_key_exists('repass', $hiba) ? $hiba['repass'] : "";
            $form .= '</div>
         </fieldset>';
            //submit
            $form .= '<div class="form-group">
<button class="btn btn-primary" name="submit" value="gyííí">Mehet</button>
</div>';
            $form .= '</form>';
            echo $form;
            echo '</div>';
            $list = '<div class="col-12"><a href="?action=new" class="btn alert alert-success">Új felvitel</a> <table class="table table-responsive table-striped centered">';
            $list .= '<tr>
                        <th>id</th>
                        <th>név</th>
                        <th>email</th>
                        <th>státusz</th>
                        <th>művelet</th>
                    </tr>';//fejléc és uj felvitel gomb
            //lekérés
            $qry = "SELECT * FROM admins";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            //eredmények bejárása
            while ($row = mysqli_fetch_assoc($result)) {
                $list .= '<tr>
                    <th>' . $row['id'] . '</th>
                    <th>' . $row['username'] . '</th>
                    <th>' . $row['email'] . '</th>
                    <th>' . $row['status'] . '</th>
                    <th><a class="btn alert alert-warning" href="?action=edit&amp;id=' . $row['id'] . '">szerkeszt</a> <a class="btn alert alert-danger" href="?action=delete&amp;id=' . $row['id'] . '">töröl</a></th>
                </tr>';
            }
            $list .= '</table>';
            echo $list;
            ?>

        </div>
    </div>
</body>
</html>