<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 05. 15.
 * Time: 11:49
 *
 */
include '../includes/connect.php';//db csatlakozás, de most a file, amit bekötünk egy mappával feljebb található egy másik mappában
include '../includes/settings.php';
include '../includes/functions.php';
session_start();//munkafolyamatok
//ha be van lépve akkor nincs dolga itt, irány az index

$hiba = [];//üres hiba tömb
if (filter_input(INPUT_POST, 'submit')!== null) {
    //echo '<pre>' . var_export($_POST, true) . '</pre>';//ez van a post tömbben
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $pass = md5(filter_input(INPUT_POST, 'password') . $secret_key);
    if (!login($email, $pass)) {
        $hiba['email'] = 'Nem megfelelő email/jelszó páros';
    }
}
if (auth()) {//ha érvényes belépés van, az indexre irányít
    header('location:index.php');
    exit();
}
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminisztráció | Belépés</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="css/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>Ruander</b> PHP tanfolyam
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Adminisztrációs felület belépés</p>

        <form method="post">
            <?php if (isset($hiba['email'])) { ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Hiba!</h4>
                    <?php echo $hiba['email']; ?>
                </div>
            <?php } ?>
            <div class="form-group has-feedback">
                <input name="email" type="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button name="submit" type="submit" class="btn btn-primary btn-block btn-flat">
                        Belépés
                    </button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <!--    <div class="social-auth-links text-center">
               <p>- OR -</p>
               <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                   Facebook</a>
               <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                   Google+</a>
           </div>
           /.social-auth-links -->

        <a href="#">Elfelejtett jelszó</a><br>
        <!--        <a href="register.html" class="text-center">Register a new membership</a>-->

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="js/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
</body>
</html>