<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 05. 15.
 * Time: 11:43
 */

//url paraméterek kinyerése (vezérlés)
$action = filter_input(INPUT_GET, 'action');
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

$hiba = [];//üres hiba tömb
if (filter_input(INPUT_POST, 'submit')) {
    //echo '<pre>' . var_export($_FILES, true) . '';//ez van a post tömbben

    //név űrlap elem hibakezelése
    $username = filter_input(INPUT_POST, 'username');
    if ($username == "") {
        $hiba['username'] = '<span class="alert alert-danger"> Kötelező kitölteni!</span>';
    }

    //email ürlap elem hibakerelése (formátum és üresség)
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="alert alert-danger"> Hibás adatformátum!</span>';
    } else {

        //nézzük meg van e ilyen már eltárolva
        $qry = "SELECT id FROM admins WHERE email ='$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        if ($row[0] && $tid != $row[0]) {

            $hiba['email'] = '<span class="alert alert-danger"> Ez az email már szerepel az adatbázisunkban!</span>';
        }
    }

    //jelszó mező hibakezelése (min 6 karakter)
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    //ha szerkesztésen vagyunk csak akkor kell ellenőrizni ha kapunk akár 1 karaktert a pass mezőben,
    if ($action == 'new' || $action == 'edit' && $pass != '') {
        if (mb_strlen($pass, 'utf8') < 6) {
            $hiba['pass'] = '<span class="alert alert-danger"> Túl kevés karakter!</span>';
        } elseif ($pass != $repass) {
            $hiba['repass'] = '<span class="alert alert-danger"> A jelszavak nem egyeztek!</span>';
        } else {
            $pass = md5($pass . $secret_key);
        }
    }

    //képfile ellenőrzés
    //min 90x90
    //csak jpg,jpeg
    if ($_FILES['avatar']['error'] == 0) {//csak akkor ellenőrzünk, ha van mit
        $info = getimagesize($_FILES['avatar']['tmp_name']);
        //var_dump($info);
        if (!$info OR $info['mime'] != 'image/jpeg') {//nem kép formátum azaz a getimagesize elhasalt vagy a mime type nem megfelelő (most csak jpeget engedünk)
            $hiba['avatar'] = 'Nem megfelelő formátum!';
        } else {//képnek kép, és jpeg formátumu, nézzük a többit
            $origWidth = $info[0];
            $origHeight = $info[1];
            //min 90 a kisebb méret
            $ratio = $origWidth / $origHeight;
            if ($ratio < 1) {//álló
                if($origWidth<90){//min méret ellenőrzés
                    $hiba['avatar'] = 'Kép túl kicsi!';
                }
                $width=90;
                $height = round($width/$ratio);
                $tajolas = 'álló';
                //crophoz eltolás számítása
                $x = 0;
                $y = round((90-$height)/2);
            } else {//fekvő vagy négyzet
                if($origHeight<90){
                    $hiba['avatar'] = 'Kép túl kicsi!';
                }
                $height = 90;
                $width = round($height*$ratio);
                $tajolas = 'fekvő v négyzet';
                //crophoz eltolás számítása
                $y = 0;
                $x = round((90-$width)/2);
            }
            //echo $tajolas.':'.$width.'|'.$height;
        }


    }
    //ha üres a hibatömb, akkor nem volt hiba
    if (empty($hiba)) {
        ///die('ok');
        //minden oké
        $now = date('Y-m-d H:i:s');
        if ($action == 'new') {
            $qry = "INSERT INTO 
                `admins` ( 
                  `username`, 
                  `email`, 
                  `pass`, 
                  `status`, 
                  `time_created`, 
                  `time_updated`) 
                  VALUES ( 
                  '$username', 
                  '$email', 
                  '$pass', 
                  '1', 
                  '$now', 
                  '$now')";
        } else {
            $qry = "UPDATE `admins` 
            SET 
                `username` = '$username', 
                `email` = '$email',
                `status` = 1,
                `time_updated` = '$now'
            WHERE 
                `id` = '$tid'";
        }
        mysqli_query($link, $qry) or die(mysqli_error($link));
        $last_insert_id = mysqli_insert_id($link) ?: $tid;
        //miután van id biztosan jöhet a képművelet ha van
        //ha van feltöltött kép
        if(is_uploaded_file($_FILES['avatar']['tmp_name'])){
            //eredeti kép memóriába
            $src_image = imagecreatefromjpeg($_FILES['avatar']['tmp_name']);
            //cél'vászon' memóriaterület kijelölése
            //1. méretarányos kicsinyítés
          /*
            $dst_image = imagecreatetruecolor($width,$height);
          */

          //2. CROP középről
            $dst_image = imagecreatetruecolor(90,90);
            //képművelet a kiszámolt paraméterekkel
            imagecopyresampled($dst_image ,$src_image,$x,$y,0 ,0,  $width ,  $height ,  $origWidth , $origHeight);
            $dir='avatars/';//ellenőrzés!!!
            $filename = 'admin-'.$last_insert_id.'.jpg';
            //header('content-type:image/jpeg');
            imagejpeg($dst_image,$dir.$filename,65);

        }
        //die('sikeres admin felvitel, az azonosító:' . $last_insert_id);
        //vissza a listázásra azaz a switch default ágára
        header('location:' . $_SERVER['PHP_SELF'] . '?page=' . $p);
        exit();
    }
}
//tartalom vezérlés alapján
switch ($action) {

    case 'delete':
        $qry = "DELETE FROM admins WHERE id = '$tid' LIMIT 1";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //vissza a listázásra azaz a switch default ágára
        header('location:' . $_SERVER['PHP_SELF'] . '?page=' . $p);
        exit();
        break;
    case 'edit':
        echo 'szerkesztés:' . $tid;
        $qry = "SELECT * FROM admins
                WHERE id = '$tid' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_assoc($result);
        if (is_array($row)) {//ha megvan akkor a rowban vannak az adatok asszociativ tömbben
            //űrlap összeállítása
            $form = '<form method="post">
    <fieldset>
        <legend>Személyes adatok:</legend>
        <div class="form-group">';
            //username
            $form .= '<label for="username">Név*</label>
        <input class="form-control" type="text" name="username" id="username" value="';
            //ha postbol jön az az elsődleges, ha nincs akkor a rowbol, ha az sincs akkor üres
            if (isset($username)) {
                $form .= $username;
            } else {
                $form .= $row['username'];
            }
            $form .= '" placeholder="John Doe">';
            $form .= isset($hiba['username']) ? $hiba['username'] : "";
            //email
            $form .= '<label for="email">Email*</label>
        <input class="form-control" type="text" name="email" id="email" value="' . (filter_input(INPUT_POST, 'email') ?: $row['email']) . '">';
            $form .= isset($hiba['email']) ? $hiba['email'] : "";
            //pass
            $form .= '<label for="pass">Jelszó*</label>
        <input class="form-control" type="password" name="pass" id="pass" value="">';
            $form .= array_key_exists('pass', $hiba) ? $hiba['pass'] : "";
            //pass retype
            $form .= '<label for="repass">Jelszó újra*</label>
        <input class="form-control" type="password" name="repass" id="repass" value="">';
            $form .= array_key_exists('repass', $hiba) ? $hiba['repass'] : "";
            $form .= '</div>
         </fieldset>';
            //submit
            $form .= '<div class="form-group">
<button class="btn btn-primary" name="submit" value="gyííí">Mehet</button>
</div>';
            $form .= '</form>';
            $output = $form;
        } else {
            $output = 'Valami nagy gond van!!! nincs meg a keresett azonosit!';
        }
        break;
    case 'new':
        //űrlap összeállítása
        $form = '<form method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Személyes adatok:</legend>
        <div class="form-group">';
        //username
        $form .= '<label for="username">Név*</label>
        <input class="form-control" type="text" name="username" id="username" value="' . (isset($username) ? $username : '') . '" placeholder="John Doe">';
        $form .= isset($hiba['username']) ? $hiba['username'] : "";
        //email
        $form .= '<label for="email">Email*</label>
        <input class="form-control" type="text" name="email" id="email" value="' . filter_input(INPUT_POST, 'email') . '">';
        $form .= isset($hiba['email']) ? $hiba['email'] : "";
        //pass
        $form .= '<label for="pass">Jelszó*</label>
        <input class="form-control" type="password" name="pass" id="pass" value=""
               required>';
        $form .= array_key_exists('pass', $hiba) ? $hiba['pass'] : "";
        //pass retype
        $form .= '<label for="repass">Jelszó újra*</label>
        <input class="form-control" type="password" name="repass" id="repass" value=""
               required>';
        $form .= array_key_exists('repass', $hiba) ? $hiba['repass'] : "";
        $form .= '</div>';
        //kép
        $form .= '<label for="avatar">Avatár</label>
        <input class="form-control" type="file" name="avatar" id="avatar">';
        $form .= isset($hiba['avatar']) ? $hiba['avatar'] : "";
        $form .= '</fieldset>';
        //submit
        $form .= '<div class="form-group">
<button class="btn btn-primary" name="submit" value="gyííí">Mehet</button>
</div>';
        $form .= '</form>';
        $output = $form;
        break;
    default:
        $list = '<div class="col-12"><a href="?page=' . $p . '&amp;action=new" class="btn alert alert-success">Új felvitel</a> <table class="table table-responsive table-striped centered">';
        $list .= '<tr>
                        <th>id</th>
                        <th>név</th>
                        <th>email</th>
                        <th>státusz</th>
                        <th>művelet</th>
                    </tr>';//fejléc és uj felvitel gomb
        //lekérés
        $qry = "SELECT * FROM admins";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        //eredmények bejárása
        while ($row = mysqli_fetch_assoc($result)) {
            $list .= '<tr>
                    <th>' . $row['id'] . '</th>
                    <th>' . $row['username'] . '</th>
                    <th>' . $row['email'] . '</th>
                    <th>' . $row['status'] . '</th>
                    <th><a class="btn alert alert-warning" href="?page=' . $p . '&amp;action=edit&amp;id=' . $row['id'] . '">szerkeszt</a> <a class="btn alert alert-danger" href="?page=' . $p . '&amp;action=delete&amp;id=' . $row['id'] . '">töröl</a></th>
                </tr>';
        }
        $list .= '</table>';
        $output = $list;
        break;
}
//output formázása admin lte sablonnak
$output = '<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        ' . $adminmenu[$p]['title'] . '
        <small>it all starts here</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="?page=' . $p . '">' . $adminmenu[$p]['title'] . '</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">' . $adminmenu[$p]['title'] . '</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            ' . $output . '
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            doboz lábléc
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>';
