<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 05. 15.
 * Time: 11:46
 */
include '../includes/connect.php';
include '../includes/settings.php';
include '../includes/functions.php';//közös eljárások gyüjteménye
session_start();//munkafolyamatok
//ha urlből kiolvassuk hogy ki akar lépni, kilépünk
//var_dump($_SESSION);die();
if (filter_input(INPUT_GET, 'action') == 'logout') {
    logout();//session aza munkafolyamat elemek rocsolása, majd utána az auth megoldja az átirányítást
}
//var_dump(auth(),$_SESSION, session_id());die();
$logged = auth();
if (!$logged){
    header('location:login.php');
    exit();//nincs belépve, irány a login
}
//url paraméterek a modulokhoz
$p = filter_input(INPUT_GET,'page',FILTER_VALIDATE_INT)?:1;//a kapott oldal azonositó vagy default 1 (ami most a dashboard)
        $modulExt='.php';
        $modulDir="modules/";
        $moduleFileName=$modulDir.$adminmenu[$p]['modulname'].$modulExt;
        //modul betöltése hiszen csak logika van benne echo nincs, és egy outputot ad ami a contentet tartalmazza
//modul tartalma
if(file_exists($moduleFileName)) {
    include $moduleFileName;//outputban kell legyen a kiírandó html
}else{
    $output = 'Nincs ilyen modul:'.$moduleFileName;
}
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Adminisztráció | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <?php
    include "template/main-header.php";
    include "template/main-sidebar.php";
    ?>
    <div class="content-wrapper">
        <?php
        //content kiírása a modulból
        echo $output;
        ?>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
    include "template/main-footer.php";
    ?>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="js/demo.js"></script>
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>
</body>
</html>
