<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php $imgSource = "avatars/admin-".$_SESSION['userdata']['id'].".jpg"; ?>
                <img src="<?php echo $imgSource;?>" class="img-circle" alt="<?php echo $_SESSION['userdata']['username'];?>">
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['userdata']['username'];?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <?php
            //sidemenu összeállítása a settingsben tárolt tömb alapján
            $sidemenu = '';
                foreach($adminmenu as $id => $element ){
                    $sidemenu .= '<li><a href="?page='.$id.'"><i class="'.$element['fa-icon'].'"></i> <span>'.$element['title'].'</span></a></li>';
                }
                echo $sidemenu;
            ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>