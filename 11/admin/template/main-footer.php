<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; <?php echo date("Y");?> <a href="https://ruander.hu" target="_blank">Ruander Oktatóközpont</a>.</strong> | PHP tanfolyam.
</footer>