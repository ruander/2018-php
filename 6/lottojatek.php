<?php

/*
 * - játék tipusának kiválasztása után (5/90, 6/45, 7/35)
 *          ----url (a tag) get metódus
 $validGameTypes = [
    5=>90,
    6=>45,
    7=>35,
]
    n db különböző egész szám a választott tipustól függő értékhatárok között (1 - 35||45||90)
    -kérjünk email címet azonosításhoz
    -rendezzük növekvő sorrendbe a tippeket hibakezelés után
    tároljuk el :
        jatektipusmappa/het.tipp
    a sorsolások az adott hetekre:
        jatektipusmappa/het.sorsolas
PL:
590/18.tipp -> itt vannak a tippek.
590/18.sorsolas -> 18. hét sorsolásának eredménye.

Sikeres tippleadáskor kapjon emailt a megadott emailcímre a tippjeivel a játékos (egy szelvényt engedünk egyszerre kitölteni)

Legyen egy sorsolási eredmények megtekintése funkció (hetekre és tipusokra bontva)

*/
//mappa létezésének ellenőrzése és ha nincs létrehozása
$dir = '590';
if(!is_dir($dir)){
    mkdir($dir,0755);
}