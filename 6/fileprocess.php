<?php
//file tratalom beolvasása 1.
$filecontent = file_get_contents('user.txt');
//$users = unserialize($filecontent); //csak 1 adat jön mert utána nem tudja értelmezni

//bontsuk vissza a PHP_EOL nél azaz sortörésenként (ezt mi tettük bele íráskor) hogy egy olyan tömböt kapjunk amelynek minden kulcsán egy adatsor található
$usersArray = explode(PHP_EOL, $filecontent);
echo '<pre>'.var_export($usersArray,true);

//járjuk be a tömböt és fejtsük vissza minden adatsorát
foreach($usersArray as $userSerialized){
    $user = unserialize($userSerialized);//egy adatsor
    var_dump($user);
}
