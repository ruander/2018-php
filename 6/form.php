<?php

//űrlap feldolgozása post használatával és hibakezelés
if (filter_input(INPUT_POST, 'submit')) {
    $hiba = [];
    echo '<pre>' . var_export($_POST, true) . '</pre>';//ez van a post tömbben
    //név
    $name = filter_input(INPUT_POST, 'nev');
    //név űrlap elem hibakezelése, nem lehet üres, és
    if ($name == "") {
        $hiba['nev'] = '<span class="error"> Kötelező kitölteni!</span>';
    }

    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //email ürlap elem hibakerelése (formátum és üresség)
    if (!$email) {
        $hiba['email'] = '<span class="error"> Hibás adatformátum!</span>';
    }

    //jelszavak
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    //jelszó mező hibakezelése (min 6 karakter)
    if (mb_strlen($pass, 'utf8') < 6) {
        $hiba['pass'] = '<span class="error"> Túl kevés karakter!</span>';
    } elseif ($pass != $repass) {//ha a pass hossza jó, egyezzenek a repass mezővel az értékei
        $hiba['repass'] = '<span class="error"> Jelszavak nem egyeznek!</span>';
    }else{
        //így biztonságosabb lehet az elavult md5 is:
        /*
         *
         $secret_key = 'ssW%_!';
        $pass = md5($pass.$secret_key);
        die($pass);
        */
        $pass = password_hash($pass,PASSWORD_BCRYPT);//biztonságos jelszó hash
        //die($pass);
    }

    //ha üres a hibatömb, akkor nem volt hiba
    if (empty($hiba)) {
        echo 'minden oké!';
        //rendszerezzük az adatokat és elkészítjük a szükséges segéd adatokat (timestamp)

        $userdata = [
            'name' => $name,
            'email' => $email,
            'pass' => $pass,
            'time_created' => time(),//'kitöltés' ideje
        ];

        //rendezett adattömb:
        echo '<pre>' . var_export($userdata, true) . '</pre>';
        //file beolvasása 1.
        //echo $test = file_get_contents('https://ruander.hu');
        //userdata sorozatosítása
        $tombsorozat = serialize($userdata);
        //die($tombsorozat);
        file_put_contents('user.txt',$tombsorozat . PHP_EOL, FILE_APPEND );
    }
}
?><!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozás html fileból (nem igazán szerencsés megoldás)</title>
</head>
<body>
<form method="post">
    <fieldset>
        <legend>Személyes adatok:</legend>
        <label for="nev">Név*</label>
        <input type="text" name="nev" id="nev" value="<?php echo filter_input(INPUT_POST, 'nev'); ?>"
               placeholder="John Doe">
        <?php echo isset($hiba['nev']) ? $hiba['nev'] : ""; ?>
        <br><label for="email">Email*</label>
        <input type="text" name="email" id="email" value="<?php echo filter_input(INPUT_POST, 'email'); ?>">
        <?php echo isset($hiba['email']) ? $hiba['email'] : ""; ?>

        <br><label for="pass">Jelszó*</label>
        <input type="password" name="pass" id="pass" value=""
        ><?php echo isset($hiba['pass']) ? $hiba['pass'] : ""; ?>
        <br><label for="repass">Jelszó újra*</label>
        <input type="password" name="repass" id="repass" value=""
        ><?php echo isset($hiba['repass']) ? $hiba['repass'] : ""; ?>
    </fieldset>
    <button name="submit" value="gyííí">Mehet</button><!--újabb fajta submit megoldás-->
</form>
</body>
</html>