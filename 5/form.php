<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 04. 25.
 * Time: 11:43
 */
echo '<pre>' . var_export($_POST, true) . '</pre>';
//űrlap feldolgozása szuperglobális tömbökkel
//csak akkor dolgozzunk fel bármit is ha az létezik
//if(isset($_POST['submit'])) { nem szép közvetlen kulcsról szuperglobális get,post,request tömbökből értéket kiolvasni) inkább filter_input
if (filter_input(INPUT_POST, 'submit')) {
    $hiba = [];
    echo '<pre>' . var_export($_POST, true) . '</pre>';//ez van a post tömbben

    //név űrlap elem hibakezelése
    if (filter_input(INPUT_POST, 'nev') == "") {
        $hiba['nev'] = '<span class="error"> Kötelező kitölteni!</span>';
    }

    //email ürlap elem hibakerelése (formátum és üresség)
    if (!filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL)) {
        $hiba['email'] = '<span class="error"> Hibás adatformátum!</span>';
    }

    //jelszó mező hibakezelése (min 6 karakter)
    $pass = filter_input(INPUT_POST, 'pass');
    if (mb_strlen($pass, 'utf8') < 6) {
        $hiba['pass'] = '<span class="error"> Túl kevés karakter!</span>';
    }


    //ha üres a hibatömb, akkor nem volt hiba
    if (empty($hiba)) {
        echo 'minden oké!';
        die();
    }

}
?><!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozás html fileból (nem igazán szerencsés megoldás)</title>
</head>
<body>
<form method="post">
    <fieldset>
        <legend>Személyes adatok:</legend>
        <label for="nev">Név*</label>
        <input type="text" name="nev" id="nev" value="tesztnév" placeholder="John Doe">
        <?php echo isset($hiba['nev']) ? $hiba['nev'] : ""; ?>
        <br><label for="email">Email*</label>
        <input type="text" name="email" id="email" value="a@a.aa">
        <?php echo isset($hiba['email']) ? $hiba['email'] : ""; ?>

        <br><label for="pass">Jelszó*</label>
        <input type="password" name="pass" id="pass" value="1234"
               required><?php echo isset($hiba['pass']) ? $hiba['pass'] : ""; ?>
    </fieldset>
    <input type="submit" name="submit" value="gyííí">
    <br><br>
    <button name="submit" value="gyííí">Mehet</button><!--újabb fajta submit megoldás-->
</form>
</body>
</html>