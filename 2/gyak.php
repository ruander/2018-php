<?php
//fejléces táblázat kialakítása
$output= '<table border="1">
		<tr>
		 <th>id</th>
		 <th>name</th>
		 <th>email</th>
		 <th>action</th>
		</tr>';//táblázat kezdés és fejléc sor kialakítása és eltárolása az output változóba
for($id=1;$id<=6;$id++){
	$output .=  '<tr>
			 <td>'.$id.'</td>
			 <td>name '.$id.'</td>
			 <td>email'.$id.'@valami.test</td>
			 <td>edit | delete</td>
		</tr>';//sorok kiírása
}
		
$output.='</table>';//táblázat lezárása

echo $output;//kiírás 
/** érdekesség
$x = 3;
$y = 5;
$x=$x+$y;//8
$y=$x-$y;//3
$x=$x-$y;//5
//.......
echo "X értéke $x , Y értéke $y"; //"X értéke 5 , Y értéke 3"
*/

//beágyazott ciklus
$sakktabla = '<table border="1">';
for( $sor=1 ; $sor<=8 ; $sor++ ){
	$sakktabla .= '<tr>';
		//beaágyazott ciklus az oszlopok (cellák) kialakítására
		for($oszlop='A';$oszlop<='H';$oszlop++){
			$sakktabla.='<td>sor: '.$sor.' | cella: '.$oszlop.'</td>';
		}
	$sakktabla .='</tr>';
}
$sakktabla.='</table>';
echo $sakktabla;
//szines táblázatok 
$colortable = '<table border="0" style="width:100%;border-collapse:collapse">';
for( $sor=1 ; $sor<=35 ; $sor++ ){
	$colortable .= '<tr>';
		//beaágyazott ciklus az oszlopok (cellák) kialakítására
		for($oszlop=1;$oszlop<=40;$oszlop++){
			$colortable.='<td style="background:rgb('.rand(0,255).','.rand(0,255).','.rand(0,255).')">&nbsp;</td>';
		}
	$colortable .='</tr>';
}
$colortable.='</table>';
echo $colortable;
//8.
$n=rand(10,20);
for( $sor=1 ; $sor<=$n ; $sor++ ){
	echo 'XO';
}
echo '<br>';
