<!doctype html>
<html lang="hu">
 <head>
	<title>Tömbök 1</title>
	<meta charset="utf-8">
 </head>
 <body>
 <h1>Egyszerű egydimenziós tömbök</h1>
 <?php
 $tomb = array();//üres tömb
 //echo $tomb; //Array
 var_dump($tomb);//változó infórmációi
 //tömb létrehozása kezdő értékekkel
 $gyumolcsok = array('körte','alma','szőlő');
 echo '<pre>';
 var_dump($gyumolcsok);
 
 $gyumolcsok[]='paradicsom';//érték hozzáadása automatikus indexre
 $gyumolcsok[100]='labda';//érték hozzáadása megadott indexre
 $gyumolcsok[]='banán';//érték hozzáadása automatikus indexre
 $gyumolcsok['csonthejas']='dió';//asszociatív index
 $gyumolcsok[]='banán';//érték hozzáadása automatikus indexre
 var_dump($gyumolcsok);
 //tömb 1 elemének megszüntetése
 $gyumolcsok[101]=NULL;//ez nem szűnteti meg! 
 unset($gyumolcsok[102]);//megszunteti
 unset($gyumolcsok[101]);//megszunteti
 $gyumolcsok[]='banán';//érték hozzáadása automatikus indexre
 var_dump($gyumolcsok);
 echo '</pre>';
//tömb műveletek
echo $gyumolcsok[1];//egy elem kiírása ha az primitív tipus
echo '<br>$gyumolcsok elemszáma: '. count($gyumolcsok);
//tömb bejárása

foreach($gyumolcsok as $key => $value){//minden elemnél az aktuális kulcs és érték párost érjük el az itt megadott változókban a key mindig primitív
	echo '<br>aktuális kulcs:'.$key.' | aktuális érték:'.$value;
}
//tömb felvétele default értékekel irányított indexekre
$user = [
	'id' => 1,
	'email' => 'hgy@ruander.hu',
	'admin' => true,
	'last_login' => date('Y-m-d H:i:s'),	
];
echo '<pre>'.var_export($user,true).'</pre>';

 ?>
 </body>
</html>