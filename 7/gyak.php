<?php
/**
 * Created by Ruander Oktatócentrum.
 * User: hgy
 * Date: 2018. 05. 07.
 * Time: 9:10
 */
//kapunk e url ben paramétert, ha igen tároljuk el egy változóba
$page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT) ?: 1;//ha nem kapunk, legyen default 1 azaz a kezdőlap
var_dump($page);
$menu = [
    1 => 'Kezdőlap',
    2 => 'Rólunk',
    3 => 'Hírek',
    4 => 'Kapcsolat',
    5 => 'Referenciák',
];
$mainmenu = '<nav><ul>';
foreach ($menu as $key => $elem) {
    $active = '';
    if ($page == $key) {
        $active = 'active';
    }
    $mainmenu .= '<li class=" ' . $active . '"><a href="?page=' . $key . '">' . $elem . '</a></li>';
}
$mainmenu .= '</ul></nav>';

echo $mainmenu;

//oldaltartalmakat tartalmazó tömb implementálása inlcude segítségével
include 'filegyak.php';//mintha ide lenne gépelve
//rendelkezésre áll a teljes $pageContent tömb az includeolt fileból, ha az létezik az adott kulcson
if (array_key_exists($page, $pageContent)) {
    $pageTitle = $pageContent[$page]['title'];
    $pageBody = $pageContent[$page]['body'];
    echo '<h1>' . $pageTitle . '</h1>';
    echo '<p>' . $pageBody . '</p>';
}else{
    echo '<h2>404 van haver...</h2>';
}
