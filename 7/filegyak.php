<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 05. 07.
 * Time: 10:16
 */
$pageContent = [
    1 => [
        'title' => 'Kezdőlap lorem ipsum dolor sit amet...',
        'body' => 'Lórum ipse, a risás resznyés samba lesz az ormány, ma telyk fedics. A másik pedig: 8 és 11 jesztő figyelésük között például megint van egy másik kírász, amikor a vásított recsüléc, a nagyon könnyen ninkedik, úgy ninkedik, hogy később elő is újszerű.'
    ],
    2 => [
        'title' => 'Rólunk lorem ipsum dolor sit amet...',
        'body' => 'Lórum ipse, a risás resznyés samba lesz az ormány, ma telyk fedics. A másik pedig: 8 és 11 jesztő figyelésük között például megint van egy másik kírász, amikor a vásított recsüléc, a nagyon könnyen ninkedik, úgy ninkedik, hogy később elő is újszerű. Fingy: - egyező szúnyosról azt nagyon sokan tudják, hogy a vikes idés agságnak a södelvélye, nagyon sok sadály ferenese. Azt már kevesebben, hogy csafékkal izzad, rösítnie vényesegíti őket, orgatott sziséregekre vényesegíti őket. Ott van az a külő szégyen figyelező, ami egy fátozatlan katásból és egy maradásból, közte éltesély salatírákból tékoz, ezt rotmárnak koppanják, ezt nokánya.'
    ],
    3 => [
        'title' => 'Hírek lorem ipsum dolor sit amet...',
        'body' => ' Fingy: - egyező szúnyosról azt nagyon sokan tudják, hogy a vikes idés agságnak a södelvélye, nagyon sok sadály ferenese. Azt már kevesebben, hogy csafékkal izzad, rösítnie vényesegíti őket, orgatott sziséregekre vényesegíti őket. Ott van az a külő szégyen figyelező, ami egy fátozatlan katásból és egy maradásból, közte éltesély salatírákból tékoz, ezt rotmárnak koppanják, ezt nokánya.'
    ],
    4 => [
        'title' => 'Kapcsolat lorem ipsum dolor sit amet...',
        'body' => 'Ez megint kissé később aztán gyaszopol, természetesen a vásított recsülécöt lehet kéletszeznie később is, de nehezebben, mint ilyen raplis figyelésben, tehát van a szolnának, olyan cikorai vannak, amikor feltétlenül téltő többi jesztő szivákkal a kória és a szemlítést ténylegesen nagyon komolyan céláznia. - fedékből mindig van regenség, hogyha van kába, akkor mindig gelkerjegteznek fel regenséget.'
    ],
    5 => [
        'title' => 'Referenciák',
        'body' => 'Bla bla'
    ]
];
/*echo '<pre>';
var_dump($pagecontent);*/