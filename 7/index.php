<?php

//lottójáték
//ha nem kapunk urlből játéktipust akkor legyen default 5
$gameType = filter_input(INPUT_GET, 'gametype', FILTER_VALIDATE_INT);
/* érvényes játéktipusok config tömbje*/
$validGameTypes = [
    5 => 90,
    6 => 45,
    7 => 35,
];
//ha vannak post adatok akkor hibakezeljük az adatokat
if (!empty($_POST)) {
    echo '<pre>' . var_export($_POST, true) . '</pre>';//ez van a postban
    $hiba = [];//üres hibatömb
    //email ellenőrzés
    $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
    if(!$email){
        $hiba['email']='Adat nem értelmezhető!';
    }
    //tippek ellenőrzés, a hibákat a $hiba['tippek'][n]
    $limit = $validGameTypes[$gameType];
    foreach ($_POST['tippek'] as $tipp => $value) {
        if($value < 1 || $value > $limit ){
             $hiba['tippek'][$tipp] = 'Adat nem értelmezhető!';
         }
    }
    var_dump($hiba);

    if(empty($hiba)){
        die("minden ok");
    }

}
if (!$gameType) {//csak akkor tegyünk ki választó menüt játéktipusra, ha még nem választott azaz nem kapunk az urlből
    $gameMenu = '<nav><ul>';
    foreach ($validGameTypes as $huzasok_szama => $limit) {

        $gameMenu .= '<li><a href = "?gametype=' . $huzasok_szama . '" > ' . $huzasok_szama . ' / ' . $limit . ' lottójáték </a ></li>';
    }
    $gameMenu .= '</ul></nav>';
    echo $gameMenu;
} else {//ha van játéktípus, ki tudjuk tenni az űrlapot, amelynek megvalósításához a form változót vezetjük be és minden ujabb elemét hozzáfűzzük a meglévőekhez
    //ha a kapott gametype nem érvényes akkor visszegyün ka választómenübe mert valami gond történt
    if (!array_key_exists($gameType, $validGameTypes)) {
        //file átirányítása
        header('location:index.php');
        exit();
    }
    $form = '<h2> ' . $gameType . ' / ' . $validGameTypes[$gameType] . ' lottójáték</h2>
            <form method="post">';
//email
    $form .= '<label for="email">email</label>';
    $form .= '<input type="email" name="email" id="email" placeholder="xy@cim.hu">'
    .hibakiir('email');
//tippek
    $form .= '<fieldset>
                <legend>Tippek megadása</legend>';
    for ($i = 1; $i <= $gameType; $i++) {
        $form .= '<br><label for="tipp-' . $i . '">' . $i . '. tipp</label>';
        $form .= '<input type="text" id="tipp-' . $i . '" name="tippek[' . $i . ']">'
        .hibakiir(['tippek'=>$i]);
    }
    $form .= '</fieldset>';
    $form .= '<button name="submit">Mehetnek a tippek</button>
            </form>';
    echo $form;
}

function hibakiir($keys){
    global $hiba;
    if(is_array($keys)){
        foreach($keys as $key => $subkey){
            $msg = isset($hiba[$key][$subkey])? $hiba[$key][$subkey]:'';
        }
    }else{
        $msg = array_key_exists($keys,$hiba)? $hiba[$keys]:'';
    }
    return $msg;
}