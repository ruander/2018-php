<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 05. 09.
 * Time: 10:25
 */
$year = 2018;
$week = 19;
$gameType = 5;
$validGameTypes = [
    5 => 90,
    6 => 45,
    7 => 35,
];
$dir = 'tippek'.'/'.$year.'/'.$week.'/';
$tippekFilename = 'gametype-'.$gameType.'.json';

//ha már létezik file olvassuk be a tartalmát és alakítsuk vissza tömbbé
if(file_exists($dir.$tippekFilename)){

    $usersJson = file_get_contents($dir.$tippekFilename);
    $usersArray = json_decode($usersJson,true);

    //sorsolás
    $sorsolas = sorsolas($gameType,$validGameTypes[$gameType]);//sorsolás a szükséges játéktipushoz
    //bejárjuk az összes tippsort
    $winners = [];
    foreach($usersArray as $id => $tippsor){
        //találatszámítás, min 2 találat nyertes, azokat eltároljuk
        $talalatok = array_intersect($sorsolas,$tippsor['tippek']);
        if(count($talalatok) >= $gameType-3){
            $winners[$id]=$tippsor;
        }
    }
    //echo '<pre>' . var_export($usersArray, true) . '</pre>';
    //echo '<pre>' . var_export($sorsolas, true) . '</pre>';
    //echo '<pre>' . var_export($winners, true) . '</pre>';
    $winnersFilename = 'winners-'.$gameType.'.json';
    $winnersJson = json_encode($winners);
    file_put_contents($dir.$winnersFilename,$winnersJson);
    die('Sorsolás kész!');
}

/**
 * saját eljárás a sorsolásra hogy többször felhasználható kódblokká váljon
 * @param int $huzasok_szama
 * @param int $limit
 * @return array
 */
function sorsolas($huzasok_szama = 5, $limit = 90)
{
    $szamok = [];//üres tömb a számoknak
    if ($huzasok_szama <= $limit) {//ne lehessen végtelen ciklus
        while (count($szamok) < $huzasok_szama) {
            $szamok[] = rand(1, $limit);
            $szamok = array_unique($szamok);//a tömb legyen a saját maga egyedi változata aza 'kivesszük az ismétlődő elemet)
        }
        sort($szamok);
    } else {
        trigger_error('Rossz paraméterezés a sorsolas($huzasok_szama = 5 , $limit = 90) eljárásnál!');//trigger error mert valami baj volt és ezt egy notice al jelezni is akarjuk (haladó)
    }
    return $szamok;//térjünk vissza a generált tömbbel
}