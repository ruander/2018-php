<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 05. 08.
 * Time: 9:56
 */
require "includes/functions.php";
//echo '<pre>' . var_export($_POST, true) . '</pre>';
//kiolvassuk az urlből a játéktipust
$gameType = filter_input(INPUT_GET, 'gametype', FILTER_VALIDATE_INT);
/* érvényes játéktipusok config tömbje húzások száma => maximális kiválasztható érték*/
$validGameTypes = [
    5 => 90,
    6 => 45,
    7 => 35,
];
//ha van kiválasztott érvényes játéktipus akkor kialakitjuk a címeket (title és h1 tartalma)
$pageTitle = '';
if (array_key_exists($gameType, $validGameTypes)) {
    $pageTitle .= $gameType . '/' . $validGameTypes[$gameType];
} elseif ($gameType) {
    //file átirányítása ha nem érvényes a játéktipus
    header('location:' . $_SERVER['PHP_SELF']);
    //echo '<pre>'.var_export($_SERVER,true);//szerverből (apache) kiolvasható adatok szuperglobális tömbje
    exit();
}
/*if($gameType and !array_key_exists($gameType, $validGameTypes)){
    die('GEBASZ');
}*/
$pageTitle .= ' Lottójáték!';
//hibakezelés mert itt már biztosan jo adat jön az urlből ha jön azaz a poston is érdemes vizsgálódni
$hiba = [];
if (!empty($_POST)) {
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<small class="form-text alert alert-danger">Adat nem értelmezhető!</small>';
    }
    //tippek ellenőrzése
    $limit = $validGameTypes[$gameType];
    $eredeti_tippek = $_POST['tippek'];//filterezz majd!!
    $egyedi_tippek = array_unique($eredeti_tippek);
    foreach ($_POST['tippek'] as $tipp => $value) {//szebb ha filterezünk
        if ($value < 1 || $value > $limit) {
            $hiba['tippek'][$tipp] = '<small class="form-text alert alert-danger">Adat nem értelmezhető!</small>';
        } elseif (!array_key_exists($tipp, $egyedi_tippek)) {//ismétlődő tippek problémájának megoldása
            $hiba['tippek'][$tipp] = '<small class="form-text alert alert-danger">Ismétlődő tipp!</small>';
        }//tippellenőrzés vége
    }//end foreach
    //terms
    if(!filter_input(INPUT_POST,'terms')){
        $hiba['terms']='<small class="form-text alert alert-danger">Kötelező elfogadni!</small>';
    }

    if (empty($hiba)) {
        //tárolás
        //$email, $egyedi_tippek
            $userdata = [
                'email' => $email,
                'tippek' => $egyedi_tippek,
            ];
            $dir = 'tippek'.'/'.date('Y').'/'.date('W').'/';
            if(!is_dir($dir)){//ha nem létezik hozzuk létre rekurzívan
                mkdir($dir,0755,true);
            }
            $filename = 'gametype-'.$gameType.'.json';
            $usersArray = [];//itt lesznek a tippek
           /* [
                    0 => [
                            'email' => 'a@a.aa',
                            'tippek' => [...]
                        ],
                    ...
            ]*/
            //ha már létezik file olvassuk be a tartalmát és alakítsuk vissza tömbbé
            if(file_exists($dir.$filename)){
                $usersJson = file_get_contents($dir.$filename);
                $usersArray = json_decode($usersJson,true);
            }

            $usersArray[] = $userdata;//adjuk hozzás a mostani tippeket a már meglévőkhöz

            $jsonData= json_encode($usersArray);//kodoljuk vissza a bővitett halmazt tárolható formába és tároljuk el

        //echo '<pre>' . var_export(json_decode($jsonData,true), true) . '</pre>';
        //jsondata string , tehát fileban tárolható
        file_put_contents($dir.$filename,$jsonData);
        //minden kész, indítsuk újra a folyamatot
        header('location:' . $_SERVER['PHP_SELF']);
        exit();
    }
}



?><!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title><?php echo $pageTitle ?></title>
</head>
<body>
<h1><?php echo $pageTitle ?></h1>
<div class="container">
    <?php
    if (!$gameType) {
        include 'includes/gameMenu.php';
    } else {
        //ha van tipus akkor tegyük ki a formot
        require 'includes/dynamicForm.php';
    }
    ?>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>