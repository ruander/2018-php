<?php
$form = '<form method="post">';
//email
$form .= '<div class="form-group">
    <label for="email">Email<sup>*</sup></label>
    <input type="text" class="form-control" id="email" aria-describedby="emailHelp" placeholder="minta@email.cim" name="email" value="' . filter_input(INPUT_POST, 'email') . '">' . hibaKiir('email') . '
    </div>';
//tippek forma illesztése ciklus segitségével
$form .= '<div class="form-group">';
for ($i = 1; $i <= $gameType; $i++) {
    $form .= '<label for="tipp-' . $i . '">' . $i . '. tipp</label>';
    $form .= '<input class="form-control" type="text" id="tipp-' . $i . '" name="tippek[' . $i . ']" value="' . @$_POST['tippek'][$i] . '">' . hibaKiir($i);
    //@$_POST['tippek'][$i] -> elnyeli a hibát ha warning vagy notice lenne (@) a hibák ezzel: 1. közvetlen kulcsrol nem veszünk ki semmit (post,get és request) szuperglobális tömbökből 2. nem gyártunk hibát direkt, hogy aztán elnyeljük!
}

$form .= '</div>';
//checkbox
$form .= '<div class="form-check">
    <input type="checkbox" class="form-check-input" id="terms" name="terms">
    <label class="form-check-label" for="terms">Elmúltam 18 éves és elfogadom a szabályzatot...</label>' . hibaKiir('terms').'</div>';
//submit és end form
$form .= '<button type="submit" class="btn btn-primary">Submit</button>
</form>';
echo $form;