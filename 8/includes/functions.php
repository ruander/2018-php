<?php
//saját eljárás hibakiírásra
function hibaKiir($key)
{
    global $hiba;//globálissá tesszük hogy az eljárás 'lássa'
    //var_dump($hiba,$key);
    $msg = '';
    if (array_key_exists($key, $hiba)) { //ha benne van visszatérünk vele amit értékként találunk
        $msg = $hiba[$key];
    } elseif (isset($hiba['tippek'])) {//nem túl szép 'drótozás' ha nincs az eredeti kulcs a hibában nézzük meg nem tipp hiba e
        if (array_key_exists($key, $hiba['tippek'])) {
            $msg = $hiba['tippek'][$key];
        }
    }
    return $msg;
}