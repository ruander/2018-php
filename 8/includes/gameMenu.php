<?php
//menü kihelyezése, ha nincs még választott játéktipus
$gameMenu = '<nav><ul class="nav flex-column">';
foreach ($validGameTypes as $huzasok_szama => $limit) {
    $gameMenu .= '<li class="nav-item"><a class="nav-link" href = "?gametype=' . $huzasok_szama . '" > ' . $huzasok_szama . ' / ' . $limit . ' lottójáték </a ></li>';
}
$gameMenu .= '</ul></nav>';
echo $gameMenu;