<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 04. 23.
 * Time: 12:07
 */
//színes számok 1 - 100
//páros : piros
//3 al osztható ZÖLD
//5 el osztható KÉK
//amelyiknek több osztója van legyen a keverék szinei az osztóknak

for($i=1;$i<=100;$i++){
    $r=$i%2==0?255:0;
    $g=$i%3==0?255:0;
    $b=$i%5==0?255:0;
    echo "<h3 style=\"color:rgb($r,$g,$b);\">$i</h3>";
}