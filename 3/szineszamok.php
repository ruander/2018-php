<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 04. 23.
 * Time: 12:50
 */
for($i=1;$i<=100;$i++){
    //klasszikus elágazás
    if($i%2==0){
        $r=255;
    }else{
        $r=0;
    }
    //deafult érték majd átállítás ha kell
    $g=0;
    if($i%3==0) $g=255;//if {} nélkül az őt követő utasításra vonatkozik egyedül
    //shorten vagy short hand if
    /*
     if(condition){
        true
    }else{
        false
    }
    condition ? true : false
     */
    $b= ($i%5==0 ? 255 : 0);
    echo "<h2 style=\"color:rgb($r,$g,$b);\">$i</h2>";
}