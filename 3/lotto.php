<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 04. 23.
 * Time: 10:59
 */
$szelvenyek_szama = 10;//ennyi szelvénnyel játszunk
$szelveny = sorsolas(7, 35);
//echo '<pre>' . var_export($szelveny, true) . '</pre>';
$sorsolas = sorsolas(7, 35);
//echo '<pre>' . var_export($sorsolas, true) . '</pre>';
//találatok kiszámítása
$talalatok = array_intersect($szelveny,$sorsolas);//közös elemei a tömbnek
//echo '<pre>' . var_export($talalatok, true) . '</pre>';
//kiírjuk az eredményeket
echo '<h2>A szelvény: '.implode(', ',$szelveny).'</h2>';
echo '<h2>A sorsolás: '.implode(', ',$sorsolas).'</h2>';
if(empty($talalatok)){//ha üres a találatok tömb
    echo '<h2>Fityisz</h2>';
}else {
    echo '<h2>A találatok: ' . implode(', ', $talalatok) . '</h2>';
}
//$a=5;
//echo sorsolas('Gyuri');
//function készítésének bemutatása, paraméter átadás és lokális változókezelés bemutatása
///**
// * Sorsolás eljárás a generálásokhoz
// * @param string $test
// * @return string
// */
//function sorsolas($test = 'world'){//az átadott paraméter nevén találjuk meg az átadott adat értékét ($test)
//
//    return 'helo '.$test;
//}
////
/**
 * saját eljárás a sorsolásra hogy többször felhasználható kódblokká váljon
 * @param int $huzasok_szama
 * @param int $limit
 * @return array
 */
function sorsolas($huzasok_szama = 5, $limit = 90)
{
    $szamok = [];//üres tömb a számoknak
    if ($huzasok_szama <= $limit) {//ne lehessen végtelen ciklus
        while (count($szamok) < $huzasok_szama) {
            $szamok[] = rand(1, $limit);
            $szamok = array_unique($szamok);//a tömb legyen a saját maga egyedi változata aza 'kivesszük az ismétlődő elemet)
        }
        sort($szamok);
    } else {
        trigger_error('Rossz paraméterezés a sorsolas($huzasok_szama = 5 , $limit = 90) eljárásnál!');//trigger error mert valami baj volt és ezt egy notice al jelezni is akarjuk (haladó)
    }
    return $szamok;//térjünk vissza a generált tömbbel
}
