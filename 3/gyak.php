<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 04. 23.
 * Time: 9:17
 */
//5 véletlenszerű szám generálása 1-90
$szelveny = [];
for($i=1;$i<=5;$i++) {

    $szam = rand(1, 90);//generálunk egy számot a határértékekre
    if(!in_array($szam,$szelveny)) {//ha benne van már a tömbben akkor nem teszem bele
        $szelveny[] = $szam;//eltároljuk a tömb következő elemének a generált számot,ha még nem volt benne
    }else{//hanem visszaléptetem a ciklusváltozót
        $i--;
    }
}

echo '<pre>'.var_export($szelveny,true).'</pre>';

//kiírjuk a generát tömb elemeit , elválasztva
sort($szelveny);//rendezzük a tömböt
echo '<h2>A szelvény: '.implode(', ',$szelveny).'</h2>';
//ugyanaz másképpen a sorsolásra

/**

$i = 1;
while(condition [$i<=5]){
    ciklusmag
 $i++;
 }

 */
$sorsolas = [];
//addig fusson a ciklus amig a !tömb elemeinek száma! (count) el nem éri az 5öt
while( count($sorsolas) < 5 ){
    $sorsolas[] = rand(1,90);
    $sorsolas = array_unique($sorsolas);//a tömb legyen a saját maga egyedi változata aza 'kivesszük az ismétlődő elemet)
}
echo '<pre>'.var_export($sorsolas,true).'</pre>';
sort($sorsolas);
echo '<pre>'.var_export($sorsolas,true).'</pre>';
echo '<h2>A sorsolás: '.implode(', ',$sorsolas).'</h2>';
