<!doctype html>
<html lang="hu">
 <head>
	<title>első php fileom</title>
	<meta charset="utf-8">
 </head>
 <body>
 <h1>Ruander Oktatóközpont - PHP alapok tanfolyam | <?php echo date("Y-m-d H:i:s"); ?></h1>
<?php
//egysoros megjegyzés
/*
több
soros
komment
*/
//változók és operátorok
//primitívek (integer,float,bool,string)
$valtozoneve = 5;//$ -> változó operátor; = -> értékadó operátor
$szam = 12;//integer
$szoveg = 'Horvath5 Gyorgy';// '' -> string operátor //string
$tort = 10/3;//float
$logikai = true;//boolean

//műveletek
//változó kiírása
echo $valtozoneve;
echo '<br>';
echo $szoveg;
echo '<br>';
echo $szam + $tort;
echo '<br>';
//echo $szoveg + $szam;//php7 től ez már warning!!!! -> szabad tipuskonverzió

$firstName = 'George';
$lastName = 'Horváth';

echo '<br>'.$firstName.' '.$lastName; // . ->konkatenátor operátor (összefűzés [string])
?>
<hr>
<h2>Elágazások és ciklusok 1.</h2>
<?php

$veletlenszam = rand(1,100);//véletlen szám generálása

echo $veletlenszam.'<br>';
//nézzük meg h páros e a generált szám
$maradek = $veletlenszam % 2; //% maradékos osztás ->és egy lépében ki is írjuk ez 2 vel valo sztás esetén 0 vagy 1 lehet

/*
if(condition){
	true
}else{
	false
}
*/
if($maradek == 0){// == értékvizsgáló operátor
	echo 'páros';
}else{
	echo 'páratlan';
}
?>
<hr>
<?php
//dobjunk egy kockával 5x, és mondjuk meg a dobások összértékét
$dobas = rand(1,6);
$dobas2 = rand(1,6);
$dobas3 = rand(1,6);
$dobas4 = rand(1,6);
$dobas5 = rand(1,6);
//fejlesztésre 'dumpoljuk' ki a változóinkat
var_dump($dobas,$dobas2,$dobas3,$dobas4,$dobas5);//csak fejlesztés közben!
$eredmeny = $dobas + $dobas2 + $dobas3 + $dobas4 + $dobas5;
echo $eredmeny;
//ugyanez ciklus segítségével
$sum = 0;
/*
for(ciklusváltozó kezdeti értéke ; ciklusváltozó vizsgálata; ciklusváltozó léptetése){
	ciklusmag
}
*/
for( $i=1 ; $i<=5 ; $i++ ){// $i++ -> $i=$i+1
	//ciklusmag
	$dobas = rand(1,6);
	echo '<br>dobás ('.$i.') = '.$dobas;
	//növeljük a sum értékét az aktuális dobás értékével
	$sum = $sum + $dobas;
}//ciklus vége

echo "<br>Dobások összértéke: \$sum = $sum";// \ -> escape operátor kiveszi a nyelvi végrehajtásból a következő karaktert - maga az escape karakter nem látszódik
?></body>
</html>