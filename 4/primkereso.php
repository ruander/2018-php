<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 04. 25.
 * Time: 9:06
 */
//primkeresés
$limit = 1000;
$a = 1;

echo $start = microtime(true);
$primek = primkeres(2,30);
echo '<h2>A primek a kért intervallumban:</h2>'.implode(', ',$primek);
echo 'Ennyi prim volt:'.count($primek);
echo '<h2>A primek a kért intervallumban, fordított sorrendben:</h2>'.implode(', ',array_reverse($primek));
//echo $stop = microtime(true);
//echo "<br>futásidő: " . ($stop - $start) . " mp";

function primkeres($tol = 2, $ig = 1000)
{
    $primek=[];
    if ($tol > $ig) {
        $a = $tol;
        $tol = $ig;
        $ig = $a;
    }

    for ($i = $tol; $i <= $ig; $i++) {
        $j = 2;
        $found = false;
        while ($j <= $i / 2 and !$found) {
            if ($i % $j == 0 && $j < $i) {
                $found = true;
            }
            $j++;
        }
        if (!$found) {
            $primek[]=$i;//tegyük a tömbbe
        }
    }
    return $primek;//array
}